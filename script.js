let texto = document.getElementById("txt")
let itens = document.querySelectorAll("#lista li")
let tab = []
let index;
         
             
             for(var i = 0; i < itens.length; i++){//faz o LI clicavel
                 tab.push(itens[i].innerHTML);
             }
             
             for(var i = 0; i < itens.length; i++){//O valor clicado retorna no Input
                 
                 itens[i].onclick = function(){
                     index = tab.indexOf(this.innerHTML);
                     console.log(this.innerHTML + " INDEX = " + index);
                     texto.value = this.innerHTML;
                 };
                 
             }
            
            function resetaArray()
            {// reseta o array
                tab.length = 0;
                itens = document.querySelectorAll("#lista li");
                // ocupa o array
                for(var i = 0; i < itens.length; i++){
                 tab.push(itens[i].innerHTML);
               }
            }
            function adicionar(){//Cria uma nova LI
                
                var listNode = document.getElementById("lista"),
                    textNode = document.createTextNode(texto.value),
                    liNode = document.createElement("LI");
                    
                    liNode.appendChild(textNode);
                    listNode.appendChild(liNode);
                    
                    resetaArray();
                    
                    
                    liNode.onclick = function(){//faz com que o novo LI vá para o input
                     index = tab.indexOf(liNode.innerHTML);
                     console.log(liNode.innerHTML + " INDEX = " + index);
                     texto.value = liNode.innerHTML;
                 };
                    
             }
             
             function editar(){//edita o LI selecionado 
                 itens[index].innerHTML = texto.value;
                 resetaArray();
              }
              
              function deletar(){//deleta o LI selecionado
                  
                      resetaArray();
                      if(itens.length > 0){
                          itens[index].parentNode.removeChild(itens[index]);
                          texto.value = "";
                      }
              }